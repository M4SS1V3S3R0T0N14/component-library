import type { PluginObject } from 'vue'

import { use } from '@/utils/plugins'
import * as components from '@/components'

// Full library plugin install function
const WiviUI: PluginObject<any> = {
  install (Vue) {
    Object.entries(components).forEach(([name, component]) => {
      Vue.component(name, component)
    })
  }
}

// Auto install library outside of a module system, that it will
// automatically install itself without the need to call Vue.use()
use(WiviUI)

// Default export is library as a whole, registered via Vue.use()
export default WiviUI
// To allow individual component use, export components
// each can be registered via Vue.component()
export * from '@/components'
