module.exports = {
  addons: [
    '@storybook/addon-knobs',
    '@storybook/addon-actions',
    'storybook-readme',
    '@storybook/addon-viewport',
    '@storybook/addon-jest'
  ]
}
