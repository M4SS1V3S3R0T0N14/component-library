# Grilles et espacements

L'organisation de l'espace est la clé de toute grande conception. 
Les systèmes spatiaux, les grilles et les dispositions fournissent des règles qui 
donnent à vos conceptions un rythme cohérent, contraignent la prise de décision et 
aident les équipes à rester alignées. Cet échafaudage fondamental est une exigence 
pour tous les systèmes de conception. 

Dans ce guide, nous allons parcourir les bases de la définition des unités de base spatiales, de la création de règles de relation avec les grilles et de l'ensemble pour les dispositions d'interface utilisateur modernes.

## Définition du système spatial 

La définition d'une unité de base permet de créer l'échelle des tailles prises en charge dans les grilles, les espacements et la typographie.

| Nom           | Variable CSS        | Valeur par défaut  |
|---------------|---------------------|--------------------|
| Unité de base | `--w-base-scale`      | `4`                |

<div style="display: flex; align-items: flex-end; justify-content: center; flex-wrap: nowrap; margin: var(--w-space-lg) 0;">
    <div style="margin: 12px; background: var(--w-color-primary); width: var(--w-space-xxxs); height: var(--w-space-xxxs);"></div>
    <div style="margin: 12px; background: var(--w-color-primary); width: var(--w-space-xxs); height: var(--w-space-xxs);"></div>
    <div style="margin: 12px; background: var(--w-color-primary); width: var(--w-space-xs); height: var(--w-space-xs);"></div>
    <div style="margin: 12px; background: var(--w-color-primary); width: var(--w-space-sm); height: var(--w-space-sm);"></div>
    <div style="margin: 12px; background: var(--w-color-primary); width: var(--w-space-md); height: var(--w-space-md);"></div>
    <div style="margin: 12px; background: var(--w-color-primary); width: var(--w-space-lg); height: var(--w-space-lg);"></div>
    <div style="margin: 12px; background: var(--w-color-primary); width: var(--w-space-xl); height: var(--w-space-xl);"></div>
    <div style="margin: 12px; background: var(--w-color-primary); width: var(--w-space-xxl); height: var(--w-space-xxl);"></div>
    <div style="margin: 12px; background: var(--w-color-primary); width: var(--w-space-xxxl); height: var(--w-space-xxxl);"></div>

</div>


| Nom          | Variable CSS        | Valeur par défaut  (calculée en px)  | Valeur par défaut               |
|--------------|---------------------|---------------------------|---------------------------------|
| Espace XXXS        | `--w-space-xxxs`      | `4px`                    | `calc(1 * var(--w-base-scale-rem))`   |
| Espace XXS  | `--w-space-xxs` | `8px`                    | `calc(2 * var(--w-base-scale-rem))`   |
| Espace XS  | `--w-space-xs` | `12px`                    | `calc(3 * var(--w-base-scale-rem))` |
| Espace SM  | `--w-space-sm` | `24px`                    | `calc(6 * var(--w-base-scale-rem))`   |
| Espace MD  | `--w-space-md` | `32px`                    | `calc(8 * var(--w-base-scale-rem))`   |
| Espace LG  | `--w-space-lg` | `48px`                    | `calc(12 * var(--w-base-scale-rem))`   |
| Espace XL  | `--w-space-xl` | `64px`                    | `calc(16 * var(--w-base-scale-rem))`   |
| Espace XXL  | `--w-space-xxl` | `80px`                    | `calc(20 * var(--w-base-scale-rem))`   |
| Espace XXXL  | `--w-space-xxxl` | `96px`                    | `calc(24 * var(--w-base-scale-rem))`   |
