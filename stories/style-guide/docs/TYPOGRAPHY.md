# Typographie
La typographie est l'un des aspects les plus importants du système de conception.
Pensez à la quantité d'informations que vous communiquerez aux utilisateurs.
Environ 85 à 90% transitent par le biais de la typographie

La typographie est souvent l'un des composants fondamentaux sur lesquels il faut se familiariser, car elle doit fonctionner harmonieusement avec d'autres éléments tels que les icônes et les contrôles de l'interface utilisateur. 
Les hauteurs de ligne peuvent influencent de nombreux autres éléments structurels comme l'espacement et les grilles.

## Styles de caractères

| Nom          | Variable CSS           | 
|--------------|---------------------   |
| <span style="font: var(--w-text-title);">Titre</span>        | `--w-text-title`      | 
| <span style="font: var(--w-text-subtitle-1);">Sous-titre 1</span> | `--w-text-subtitle-1` | 
| <span style="font: var(--w-text-subtitle-2);">Sous-titre 2</span> | `--w-text-subtitle-2` | 
| <span style="font: var(--w-text-subtitle-3);">Sous-titre 3</span> | `--w-text-subtitle-3` | 
| <span style="font: var(--w-text-headline);">Headline</span>     | `--w-text-headline`   | 
| <span style="font: var(--w-text-paragraph);">Paragraph</span>    | `--w-text-paragraph`  | 
| <span style="font: var(--w-text-caption);">Légende</span>       | `--w-text-caption`    | 
| <span style="font: var(--w-text-details);">Détails</span>      | `--w-text-details`    | 
| <span style="font: var(--w-text-minus);">Minus</span>         | `--w-text-minus`      | 


## Polices de caractères
De par sa nature multi-client, le Wivi met à disposition 2 choix de polices de caractère.

| Nom               | Variable CSS       | Description                                                                |
|-------------------|--------------------|----------------------------------------------------------------------------|
| <span style="font-family: var(--w-font-primary);">Police principale</span> | `--w-font-primary`   | Elle est utilisée pour les textes courants, les descriptions, les boutons… |
| <span style="font-family: var(--w-font-secondary);">Police secondaire</span> | `--w-font-secondary` | Elle est utilisée principalement pour les titres.                         |

## Tailles de police, hauteurs de ligne, graisses et réactivité

Comme vu dans le chapitre précédent, la typographie est calculée sur la valeur de base du système spatial `--w-base-scale: 4`

### Tailles de police

| Nom          | Variable CSS        | Taille en px  |
|--------------|---------------------|---------------------------|
| <span style="font: var(--w-text-title);">Titre</span>        | `--w-text-title-fs`      | `32px`                    |
| <span style="font: var(--w-text-subtitle-1);">Sous-titre 1</span> | `--w-text-subtitle-1-fs` | `20px`                    |
| <span style="font: var(--w-text-subtitle-2);">Sous-titre 2</span> | `--w-text-subtitle-2-fs` | `18px`                    |
| <span style="font: var(--w-text-subtitle-3);">Sous-titre 3</span> | `--w-text-subtitle-3-fs` | `16px`                    |
| <span style="font: var(--w-text-headline);">Headline</span>     | `--w-text-headline-fs`   | `18px`                    |
| <span style="font: var(--w-text-paragraph);">Paragraph</span>    | `--w-text-paragraph-fs`  | `16px`                    
| <span style="font: var(--w-text-caption);">Légende</span>      | `--w-text-caption-fs`    | `14px`                    |
| <span style="font: var(--w-text-details);">Détails</span>      | `--w-text-details-fs`    | `12px`                    |
| <span style="font: var(--w-text-minus);">Minus</span>        | `--w-text-minus-fs`      | `10px`                    |

### Hauteurs de ligne

| Nom          | Variable CSS           | Taille en px  |
|--------------|---------------------   |---------------------------|
| <span style="font: var(--w-text-title);box-shadow: 0 0 0 1px var(--w-color-app-2)">Titre</span>        | `--w-text-title-lh`      | `36.8px`                  |
| <span style="font: var(--w-text-subtitle-1);box-shadow: 0 0 0 1px var(--w-color-app-2)">Sous-titre 1</span> | `--w-text-subtitle-1-lh` | `24px`                    |
| <span style="font: var(--w-text-subtitle-2);box-shadow: 0 0 0 1px var(--w-color-app-2)">Sous-titre 2</span> | `--w-text-subtitle-2-lh` | `24px`                    |
| <span style="font: var(--w-text-subtitle-3);box-shadow: 0 0 0 1px var(--w-color-app-2)">Sous-titre 3</span> | `--w-text-subtitle-3-lh` | `24px`                    |
| <span style="font: var(--w-text-headline);box-shadow: 0 0 0 1px var(--w-color-app-2)">Headline</span>     | `--w-text-headline-lh`   | `24px`                    |
| <span style="font: var(--w-text-paragraph);box-shadow: 0 0 0 1px var(--w-color-app-2)">Paragraph</span>    | `--w-text-paragraph-lh`  | `24px`                    |
| <span style="font: var(--w-text-caption);box-shadow: 0 0 0 1px var(--w-color-app-2)">Légende</span>       | `--w-text-caption-lh`    | `20px`                    |
| <span style="font: var(--w-text-details);box-shadow: 0 0 0 1px var(--w-color-app-2)">Détails</span>      | `--w-text-details-lh`    | `16px`                    |
| <span style="font: var(--w-text-minus);box-shadow: 0 0 0 1px var(--w-color-app-2)">Minus</span>         | `--w-text-minus-lh`      | `14px`                    |

### Graisse

| Nom          | Variable CSS        | Valeur  |
|--------------|---------------------|---------------------------|
| <span style="font: var(--w-text-title);">Titre</span>        | `--w-text-title-we`      | `400`                    |
| <span style="font: var(--w-text-subtitle-1);">Sous-titre 1</span> | `--w-text-subtitle-1-we` | `400`                    |
| <span style="font: var(--w-text-subtitle-2);">Sous-titre 2</span> | `--w-text-subtitle-2-we` | `400`                    |
| <span style="font: var(--w-text-subtitle-3);">Sous-titre 3</span> | `--w-text-subtitle-3-we` | `700`                    |
| <span style="font: var(--w-text-headline);">Headline</span>     | `--w-text-headline-we`   | `400`                    |
| <span style="font: var(--w-text-paragraph);">Paragraph</span>    | `--w-text-paragraph-we`  | `400`                    
| <span style="font: var(--w-text-caption);">Légende</span>      | `--w-text-caption-we`    | `400`                    |
| <span style="font: var(--w-text-details);">Détails</span>      | `--w-text-details-we`    | `400`                    |
| <span style="font: var(--w-text-minus);">Minus</span>        | `--w-text-minus-we`      | `400`                    |


### Réactivité

```css
:root {
  --w-base-scale: 4; 
  @media only screen and (min-width: 480px) {
    --w-base-scale: 5.3; 
  }
}

body {
  font: var(--w-text-paragraph);
}
```
