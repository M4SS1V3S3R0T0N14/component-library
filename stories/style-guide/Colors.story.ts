import { Category } from '../storiesHierarchy'
import COLORS from './docs/COLORS.md'

export default {
  title: Category.COLORS,
  parameters: {
    viewport: false,
    options: { showAddonPanel: false },
    readme: { content: COLORS }
  }
}

export const Couleurs = () => ({})
