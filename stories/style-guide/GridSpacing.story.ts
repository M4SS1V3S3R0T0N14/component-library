import { Category } from '../storiesHierarchy'
import GRID_AND_SPACES from './docs/GRID-AND-SPACES.md'

export default {
  title: Category.GRID_AND_SPACES,
  parameters: {
    viewport: false,
    options: { showAddonPanel: false },
    readme: { content: GRID_AND_SPACES }
  }
}

export const GrillesEtEspacements = () => ({
  name: 'Grilles et espacements'
})
