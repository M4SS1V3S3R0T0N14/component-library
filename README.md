# Wivi UI ![pipeline](https://gitlab.com/M4SS1V3S3R0T0N14/test-wivi-ui/badges/master/pipeline.svg) [![Storybook](https://cdn.jsdelivr.net/gh/storybookjs/brand@master/badge/badge-storybook.svg)](https://m4ss1v3s3r0t0n14.gitlab.io/test-wivi-ui/)

Wivi UI est une bibliothèque de composants d'interface utilisateur réactifs pour Vue.js basée sur les guides de style du Wivi. 🎨 

## NPM
### Installation
Après avoir créer un projet Vue.js avec vue-cli.
```bash
npm install @guillaumecatel/wivi-ui
```

**Note importante !**
*Le tarball NPM est visible [à cette adresse](https://unpkg.com/browse/@guillaumecatel/wivi-ui/).*

### Utilisation
#### Librarie complète
```js
import Vue from 'vue'
import WiviUI from '@guillaumecatel/wivi-ui'
import '@guillaumecatel/wivi-ui/lib/wivi-ui.min.css'

Vue.use(WiviUI)
```

#### Par composant (pas de tremblement d'arbre !)
```js
import Vue from 'vue'
import { WButton } from '@guillaumecatel/wivi-ui'
import '@guillaumecatel/wivi-ui/lib/components/button.min.css'

Vue.component('WButton', WButton)
```

#### Par composant (avec tremblement d'arbre !)
```js
import Vue from 'vue'
import { WButton } from '@guillaumecatel/wivi-ui/lib/components/button'
import '@guillaumecatel/wivi-ui/lib/components/button.min.css'

Vue.component('WButton', WButton)
```


## Autonome
Utilisation de CDN ou téléchargement de fichiers pour servir localement

### Installation
- Script: [https://unpkg.com/@guillaumecatel/wivi-ui/dist/wivi-ui.min.js](https://unpkg.com/@guillaumecatel/wivi-ui/dist/wivi-ui.min.js)
- Style: [https://unpkg.com/@guillaumecatel/wivi-ui/dist/wivi-ui.min.css](https://unpkg.com/@guillaumecatel/wivi-ui/dist/wivi-ui.min.css)

### Usage

#### Librarie complète

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://unpkg.com/@guillaumecatel/wivi-ui/dist/wivi-ui.min.css">
</head>

<body>
    <div id="app">
        <w-button>Click me :)</w-button>
    </div>
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/@guillaumecatel/wivi-ui/dist/wivi-ui.min.js"></script>
    <script>
        new Vue({
            el: '#app'
        })
    </script>
</body>
</html>
```

#### Par composant

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://unpkg.com/@guillaumecatel/wivi-ui/dist/components/button.min.css">
</head>

<body>
    <div id="app">
        <w-button>Click me :)</w-button>
    </div>
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/@guillaumecatel/wivi-ui/dist/components/button.min.js"></script>
    <script>
        new Vue({
            el: '#app'
        })
    </script>
</body>
</html>
```
## Typescript
La librairie est basée sur Typescript et contient automatiquement [les fichiers de définition d.ts](https://gitlab.com/M4SS1V3S3R0T0N14/test-wivi-ui/-/tree/master/types) lorsque vous l'installez.

## Support navigateur
Les versions récentes de Firefox, Chrome, Edge, Opera and Safari. IE10+ est partiellement supporté.

## Contribuer
Rendez-vous sur le [guide des contributeurs](https://gitlab.com/M4SS1V3S3R0T0N14/test-wivi-ui/-/blob/master/CONTRIBUTING.md) ! 😃
