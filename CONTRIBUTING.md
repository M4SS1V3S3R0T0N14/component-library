# Contribuer
Merci de contribuer à Wivi UI ! 💪

## Installation
```bash
git clone https://gitlab.com/twelve-solutions/wivi/wivi-ui.git
cd wivi-ui
git checkout -b feature/<my-awesome-feature>
npm install
```

## Usage

### CLI
```bash
# lancer l'environnement de développement storybook avec le rechargement à chaud
npm run dev:storybook
```

```bash
# construire la libraire storybook
npm run build:storybook
```

```bash
# construire la librairie pour NPM
npm run build:lib
```

```bash
# lancer les tests unitaire avec Jest
npm run test:unit
```

```bash
# lancer les tests e2e avec Cypress
npm run test:e2e
```

```bash
# lancer le lint avec ESLint
npm run lint
```

### Commencer le développement avec
```bash
npm run dev:storybook
```
### Création d'un composant Vue.js
Aller dans `src/components`, créer un répertoire pour le composant `<my-component>/` et ajouter ces fichiers : `<MyComponent>.vue`, `<MyComponent>.spec.ts`, `README.md`, `index.ts`.
#### Exemple
`<MyComponentName>.vue`
```vue
<script lang="ts">
  import { Vue, Component } from 'vue-property-decorator'

  const config = {
    name: 'w-<my-component>'
  }

  @Component(config)
  export default class MyComponent extends Vue {
     msg: string = 'Hello world'
  }
</script>

<template>
    <div :class="$style.myComponent">
        <p>{{ msg }}</p>
        <input type="text" v-model="msg" />
    </div>
</template>

<style lang="scss" module>
    .my-component {
        color: #111;
    }
</style>
```

`<MyComponent>.spec.ts`
```typescript
import { shallowMount, Wrapper} from '@vue/test-utils'
import { WMyComponent } from '@/components/<my-component>'

let wrapper: Wrapper<WMyComponent>

describe('WMyComponent', () => {

  beforeEach(() => {
    wrapper = shallowMount(WMyComponent)
  })

  it('is called', () => {
    expect(wrapper.name()).toBe('w-my-component')
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('render correctly', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
})
```

`README.md`
```markdown
# My Component
```

`index.ts`
```typescript
import type { PluginObject } from 'vue'

import { use } from '@/utils/plugins'
import MyComponent from './MyComponent.vue'

// Individually plugin install function
const Plugin: PluginObject<any> = {
  install (Vue) {
    Vue.component('w-my-component', MyComponent)
  }
}

// Auto install library outside of a module system, that it will
// automatically install itself without the need to call Vue.use()
use(Plugin)

// Default export is component, registered via Vue.use()
export default Plugin
// To allow individual component use, export components
// each can be registered via Vue.component()
export { MyComponent as WMyComponent }

```

### Créer une story 
Aller dans le dossier `stories`, et ajouter un fichier `<MyStory>.story.ts`.
#### Exemple
```typescript
import { Category } from '../../storiesHierarchy'
import README from '@/components/<my-component>/README.md'

export default {
  title: `${Category.COMPONENTS}/<MyComponent>`,
  parameters: {
    readme: { sidebar: README },
    jest: ['<WMyComponent>.spec.ts']
  }
}

export const MyStoryName = () => ({
  data () {
    return {
      msg: 'Hello world'
    }
  },
  template: `
    <div>
        <p>{{ msg }}</p>
        <input type="text" v-model="msg" />
    </div>
  `
})
```
