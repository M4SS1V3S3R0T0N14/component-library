module.exports = {
  plugins: ['@babel/plugin-transform-runtime'],
  presets: [
    ['@vue/cli-plugin-babel/preset', {
      jsx: false /* Disabled due to clash with Storybook MDX */
    }]
  ]
}
